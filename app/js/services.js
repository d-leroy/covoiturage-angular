'use strict';

/* Services */

var myServices = angular.module('myServices', ['ngResource']);

myServices.factory('Crews', ['$resource',
  function($resource){
    return $resource('http://localhost:8080/rest/event/:eventId/crews', {}, {
      query: {method:'GET', isArray:true}
    });
  }]);

myServices.factory('Event', ['$resource',
  function($resource){
    return $resource('http://localhost:8080/rest/event/:eventId', {}, {});
  }]);

myServices.factory('Events', ['$resource',
  function($resource){
    return $resource('http://localhost:8080/rest/event/events', {}, {
      query: {method:'GET', isArray:true}
    });
  }]);

myServices.factory('User', ['$resource', 
  function($resource){
    return $resource('http://localhost:8080/rest/user/login/:userLogin/', {}, {});
  }]);

myServices.factory('AddEvent', ['$resource', 
  function($resource){
    return $resource('http://localhost:8080/rest/event/create/', {}, {
    	add: {method:'PUT'}
    });
  }]);

