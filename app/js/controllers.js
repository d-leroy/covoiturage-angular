'use strict';

/* Controllers */

var myControllers = angular.module('myControllers', []);

myControllers.controller('EventListCtrl', ['$scope', 'Events',
  function($scope, Events) {
    $scope.events = Events.query();
  }]);

myControllers.controller('EventDetailCtrl', ['$scope', '$routeParams', 'Crews', 'Event',
  function($scope, $routeParams, Crews, Event) {
	$scope.event = Event.get({eventId: $routeParams.eventId});
    $scope.crews = Crews.query({eventId: $routeParams.eventId});
  }]);

myControllers.controller('AddEventCtrl', ['$scope', '$routeParams', 'AddEvent',
   function($scope, $routeParams, AddEvent) {
	 $scope.addEvent = function() {
         AddEvent.add({title: $scope.title, place: $scope.place, date: $scope.date, hour: $scope.hour});
	 };
}]);