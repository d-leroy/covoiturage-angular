'use strict';

/* App Module */

var myApp = angular.module('myApp', [
  'ngRoute',

  'myControllers',
  'myServices'
]);

myApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/events', {
        templateUrl: 'partials/event-list.html',
        controller: 'EventListCtrl'
      }).
      when('/events/create', {
        templateUrl: 'partials/event-create.html',
        controller: 'AddEventCtrl'
      }).
      when('/events/:eventId', {
        templateUrl: 'partials/event-detail.html',
        controller: 'EventDetailCtrl'
      }).
      otherwise({
        redirectTo: '/events'
      });
  }]);
